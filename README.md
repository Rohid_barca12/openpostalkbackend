# Postalk

Postalk Backend is a repository containing all files associated with the workings of the Postalk projects backend.

Documentation used in the making of this project.<br>
Framework: https://docs.nestjs.com/ <br>
Auth: https://docs.nestjs.com/techniques/authentication <br>
Database: https://docs.nestjs.com/techniques/database <br>
Validation: https://docs.nestjs.com/techniques/validation <br>
Security: https://docs.nestjs.com/techniques/security