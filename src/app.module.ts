import { Module, ClassSerializerInterceptor } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UserModule } from './user/user.module'
import { AuthModule } from './auth/auth.module'
import { CategoryModule } from './category/category.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './entities/User'
import { APP_INTERCEPTOR } from '@nestjs/core'
import { Category } from './entities/Category'
import { Role } from './entities/Role'
import { Post } from './entities/Post'
import { Report } from './entities/Report'
import { Comment } from './entities/Comment'
import { RoleModule } from './role/role.module'
import { PostModule } from './post/post.module'
import { CommentModule } from './comment/comment.module'
import { ReportModule } from './report/report.module'

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'postalk',
      entities: [User, Role, Category, Post, Comment, Report],
      synchronize: true
    }),

    AuthModule,
    ReportModule,
    UserModule,
    CategoryModule,
    RoleModule,
    PostModule,
    CommentModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor
    }
  ]
})
export class AppModule { }
