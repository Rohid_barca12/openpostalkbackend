import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { AuthService } from './auth.service'
import { RegisterUserDto } from 'src/dto/register.dto'

@Controller('auth')
export class AuthController {
  constructor (
    private readonly authService: AuthService
  ) {}

  /**
   * Login's in a user with username and password.
   * Does not require any authorization.
   *
   * @param req Request
   */
  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login (@Request() req) {
    return this.authService.login(req.user)
  }

  /**
   * Registers a new user with the register user dto.
   * Does not require any authorization.
   *
   * @param register RegisterUserDto
   */
  @Post('register')
  async register (@Body() register: RegisterUserDto) {
    return this.authService.register(register)
  }
}
