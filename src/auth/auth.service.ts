import { Injectable, BadRequestException } from '@nestjs/common'
import { UserService } from '../user/user.service'
import { JwtService } from '@nestjs/jwt'
import { User } from 'src/entities/User'
import * as bcrypt from 'bcrypt'
import { RegisterUserDto } from 'src/dto/register.dto'

@Injectable()
export class AuthService {
  constructor (
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser (username: string, pass: string): Promise<any> {
    const user: User = await this.userService.getByUsername(username)

    if (!user) {
      return null
    }

    const resultBcrypt: boolean = await bcrypt.compare(pass, user.password)

    if (!resultBcrypt) {
      return null
    }

    const { password, ...result } = user
    return result
  }

  async login (user: any) {
    const payload = { id: user.id, sub: user.userId }
    return {
      access_token: this.jwtService.sign(payload)
    }
  }

  async register (register: RegisterUserDto) {
    let user: User = await this.userService.getByUsername(register.username)

    // Check if somebody has this username in use.
    if (user) {
      throw new BadRequestException('Username is already in use by another user.')
    }

    user = await this.userService.getByEmail(register.email)

    // Check if somebody has this email in use.
    if (user) {
      throw new BadRequestException('Email is already in use by another user.')
    }

    // Check if password is the same as confirm password.
    if (register.password !== register.confirmPassword) {
      throw new BadRequestException('Passwords do not match!')
    }

    register.password = await bcrypt.hash(register.password, 12)
    this.userService.save({
      firstName: register.firstName,
      lastName: register.lastName,
      username: register.username,
      email: register.email,
      password: register.password
    })
  }
}
