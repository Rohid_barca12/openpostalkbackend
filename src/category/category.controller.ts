import { Controller, Get, UseGuards, Body, Put, Post, Delete, Req, Query, Param } from '@nestjs/common'
import { CategoryService } from './category.service'
import { RangeDto } from 'src/dto/range.dto'
import { CreateCategoryDto } from 'src/dto/create.category.dto'
import { AuthGuard } from '@nestjs/passport'
import { DeleteCategoryDto } from 'src/dto/delete.category.dto'
import { OptionalParseIntPipe } from 'src/pipes/optional-parse-int.pipe'

@Controller('category')
export class CategoryController {
  constructor (private readonly categoryService: CategoryService) {}

    /**
     * Gets all categories from the database.
     */
    @Get()
    getAll (@Query('entities', new OptionalParseIntPipe()) entities: number) {
      return this.categoryService.getAll(entities)
    }

    /**
    * Gets a specific category with the specified id from
    * the route.
    *
    * Optional entities paramater can be added
    * to also include all relations into the output body.
    *
    * @param id Paramter Number
    * @param entities QueryParamater Number (Optional)
    */
    @Get(':id')
    getCategory (@Param('id') id, @Query('entities', new OptionalParseIntPipe()) entities: number) {
      return this.categoryService.getById(id, entities)
    }

    /**
     * Gets all categories from the database
     * in a certain range. Supports sorting. Check dto.
     *
     * @param body
     */
    @Get('range')
    getRange (@Body() body: RangeDto) {
      return this.categoryService.getRange(body)
    }

    /**
     * Creates a new category and saves to the database.
     * Uses CreateCategoryDto.
     *
     * @param body CreateCategoryDto.
     */
    @Post()
    @UseGuards(AuthGuard('jwt'))
    create (@Req() req, @Body() body: CreateCategoryDto) {
      return this.categoryService.create(req.user.id, body)
    }
}
