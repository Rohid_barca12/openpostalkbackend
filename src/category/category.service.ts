import { Injectable, BadRequestException } from '@nestjs/common'
import { Category } from 'src/entities/Category'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, Between } from 'typeorm'
import { CreateCategoryDto } from 'src/dto/create.category.dto'
import { RangeDto } from 'src/dto/range.dto'
import { DeleteCategoryDto } from 'src/dto/delete.category.dto'

@Injectable()
export class CategoryService {
  constructor (
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>
  ) {}

  /**
     * Gets all categories from the database.
     */
  getAll (entities: number): Promise<Category[]> {
    return entities === 1 ? this.categoryRepository.find({ relations: ['posts', 'user']}) : this.categoryRepository.find()
  }

  async create (userId: number, dto: CreateCategoryDto) {
    const category: Category = await this.getByName(dto.name)

    // Check if somebody has already created this category with this name.
    if (category) {
      throw new BadRequestException('Name is already in use by another category.')
    }

    return this.categoryRepository.save({
      name: dto.name,
      userId: userId,
      description: dto.description
    })
  }

  /**
     * Gets all categories from the database
     * in a specified range.
     */
  getRange (body: RangeDto) {
    switch (body.sort) {
      case 1:
        return this.categoryRepository.find({
          where: [{
            id: (Between(body.start, body.end))
          }],
          order: {
            createdAt: 'ASC'
          }
        })
      case 2:
        return this.categoryRepository.find({
          where: [{
            id: (Between(body.start, body.end))
          }],
          order: {
            createdAt: 'DESC'
          }
        })
      case 3:
        return this.categoryRepository.find({
          where: [{
            id: (Between(body.start, body.end))
          }],
          order: {
            updatedAt: 'ASC'
          }
        })
      case 4:
        return this.categoryRepository.find({
          where: [{
            id: (Between(body.start, body.end))
          }],
          order: {
            updatedAt: 'DESC'
          }
        })
      default:
        return this.categoryRepository.find({
          where: [{
            id: (Between(body.start, body.end))
          }]
        })
    }
  }

  /**
     * Gets specified category by name.
     */
  getByName (name: string): Promise<Category> {
    return this.categoryRepository.findOne({ name: name })
  }

  /**
   * Gets specified category by id.
   */
  getById (id: number, entities: number): Promise<Category> {
    return entities === 1 ? this.categoryRepository.findOne({ where: { id: id }, relations: ['posts', 'user'] }) : this.categoryRepository.findOne({ id: id })
  }
}
