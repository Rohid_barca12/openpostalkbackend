import { Controller, Get, Query, Param, Body, Req, Post, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { OptionalParseIntPipe } from 'src/pipes/optional-parse-int.pipe';
import { CreateCommentDto } from 'src/dto/create.comment.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('comment')
export class CommentController {
    constructor (private readonly commentService: CommentService) {}

    /**
     * Gets all comments.
     *
     * Optional entities paramater can be added
     * to also include all relations into the output body.
     *
     * @param id Identifier for the category this comment belongs to.
     */
    @Get()
    getAll (@Param('id') id: number) {     
      return this.commentService.getAll(id)
    }

    /**
     * Gets all comments.
     *
     * Optional entities paramater can be added
     * to also include all relations into the output body.
     *
     * @param id Identifier for the category this comment belongs to.
     */
    @Get(':id')
    getAllWithPost (@Param('id') id: number, @Query('entities', new OptionalParseIntPipe()) entities: number) {     
      return this.commentService.getAllWithPost(id)
    }

    /**
     * Creates a new comment and save to the database.
     * Uses CreateCommentDto.
     *
     * @param body CreateCommentDto.
     */
    @Post()
    @UseGuards(AuthGuard('jwt'))
    create (@Req() req, @Body() body: CreateCommentDto, @Query('entities', new OptionalParseIntPipe()) entities: number) {
      return this.commentService.create(req.user.id, body)
    }

}
