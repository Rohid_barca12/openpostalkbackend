import { Injectable, BadRequestException } from '@nestjs/common';
import { Comment } from '../entities/Comment';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostService } from 'src/post/post.service';
import { Post } from 'src/entities/Post';
import { CreateCommentDto } from 'src/dto/create.comment.dto';

@Injectable()
export class CommentService {
    constructor (
        @InjectRepository(Comment)
        private readonly commentService: Repository<Comment>,
        private readonly postService: PostService
    ) {}

    /**
     * Gets all posts from the database.
     */
    getAll (id: number): Promise<Comment[]> {
        return this.commentService.find()
    }
  
    /**
     * Gets all comments from the database.
     */
    getAllWithPost (id: number): Promise<Comment[]> {
      return this.commentService.find({
        where: {
          postId: id
        }
      })
    }

    async create (userId: number, dto: CreateCommentDto) {
      const post: Post = await this.postService.getById(dto.postId, 0);

      // Check if somebody is creating a post for a existing category.
      if (!post) {
        throw new BadRequestException('Post does not exist!')
      }
  
      return this.commentService.save({
          content: dto.content,
          postId: dto.postId,
          userId: userId
      })
    }
}
