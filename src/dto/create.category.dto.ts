import { MinLength, MaxLength, IsOptional } from 'class-validator'

export class CreateCategoryDto {
    @MinLength(3)
    @MaxLength(32)
    name: string;

    @IsOptional()
    description: string;
}
