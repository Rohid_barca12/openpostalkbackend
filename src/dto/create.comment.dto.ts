import { MinLength, MaxLength, IsNumber } from 'class-validator'

export class CreateCommentDto {

    @MinLength(3)
    @MaxLength(2048)
    content: string;

    @IsNumber()
    postId: number;
    
}
