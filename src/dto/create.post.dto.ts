import { MinLength, MaxLength, IsNumber } from 'class-validator'

export class CreatePostDto {
    @MinLength(3)
    @MaxLength(128)
    title: string;

    @MinLength(16)
    @MaxLength(2048)
    content: string;

    @IsNumber()
    categoryId: number;
}
