import { MinLength, MaxLength, IsOptional, IsNumber } from 'class-validator'

export class CreateReportDto {
    /**
     * Content for the report.
     */
    @MinLength(16)
    @MaxLength(1024)
    content: string;

    /**
     * User Id for report.
     * Only fill in userId for a user report.
     */
    @IsNumber()
    @IsOptional()
    userId?: number;

    /**
     * Category Id for report.
     * Only fill in categoryId for a category report.
     */
    @IsNumber()
    @IsOptional()
    categoryId?: number;

    /**
     * Post Id for report.
     * Only fill in postID for a post report.
     */
    @IsNumber()
    @IsOptional()
    postId?: number;

    /**
     * Comment Id for report.
     * Only fill in commentId for a comment report.
     */
    @IsNumber()
    @IsOptional()
    commentId?: number;
}
