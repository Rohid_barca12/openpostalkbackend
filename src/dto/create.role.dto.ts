import { MinLength, MaxLength, IsNumber } from 'class-validator'

export class CreateRoleDto {
    @MinLength(3)
    @MaxLength(32)
    name: string;
}
