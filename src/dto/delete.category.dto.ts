import { MinLength, MaxLength, IsNumber } from 'class-validator'

export class DeleteCategoryDto {
    @IsNumber()
    id: number;

    @MinLength(16)
    @MaxLength(2048)
    reason: string;
}
