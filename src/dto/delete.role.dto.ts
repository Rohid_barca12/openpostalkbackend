import { MinLength, MaxLength } from 'class-validator'

export class DeleteRoleDto {
    @MinLength(3)
    @MaxLength(32)
    id: number;

    @MinLength(16)
    @MaxLength(2048)
    reason: string;
}
