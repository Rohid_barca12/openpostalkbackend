import { MinLength, MaxLength } from 'class-validator'

export class LoginUser {
    @MinLength(5)
    @MaxLength(16)
    username: string;

    @MinLength(5)
    @MaxLength(16)
    password: string;
}
