import { IsNumber, IsOptional } from 'class-validator'

export class RangeDto {
    /**
     * Start of the range.
     */
    @IsNumber()
    start: number;

    /**
     * End of the range.
     */
    @IsNumber()
    end: number;

    /**
     * Sort.
     * This option is optional.
     *
     * 1: Sorts all categories in range by created at. (ASC)
     * 2: Sorts all categories in range by created at (DESC)
     * 3: Sorts all categories in range by updated at. (ASC)
     * 4: Sorts all categories in range by updated at. (DESC)
     */
    @IsOptional()
    @IsNumber()
    sort?: number;
}
