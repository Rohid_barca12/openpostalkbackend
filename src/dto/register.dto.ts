import { MinLength, MaxLength, IsEmail } from 'class-validator'

export class RegisterUserDto {
    @MinLength(6)
    @MaxLength(16)
    username: string;

    @MinLength(2)
    @MaxLength(64)
    firstName: string;

    @MinLength(2)
    @MaxLength(64)
    lastName: string;

    @IsEmail()
    email: string;

    @MinLength(6)
    @MaxLength(32)
    password: string;

    @MinLength(6)
    @MaxLength(32)
    confirmPassword: string;
}
