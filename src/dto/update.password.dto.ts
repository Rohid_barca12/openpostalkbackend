import { MinLength, MaxLength, Equals } from 'class-validator'

export class UpdatePasswordDto {
    @MinLength(6)
    @MaxLength(32)
    oldPassword: string;

    @MinLength(6)
    @MaxLength(32)
    newPassword: string;

    @MinLength(6)
    @MaxLength(32)
    confirmNewPassword: string;
}
