import { PrimaryGeneratedColumn, Column, Unique, Entity, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm'
import { Post } from './Post';
import { User } from './User';
import { Exclude } from 'class-transformer';

/**
 * Category (also known as a subtalk)
 */

@Entity('category')
export class Category {
    /**
     * ID of category.
     */
    @PrimaryGeneratedColumn()
    id?: number;

    /**
     * Post relation
     */
    @OneToMany((type) => Post, post => post.category)
    posts?: Post[];

    /**
     * Name is category.
     */
    @Column({ type: 'varchar', length: 45 })
    name: string;

    /**
     * Name is category.
     */
    @Column({ type: 'varchar', length: 45, nullable: true })
    description?: string;

    @ManyToOne((type) => User)
    @JoinColumn()
    user?: User;

    @Column()
    @Exclude()
    userId: number;

    /**
     * Created at date of user.
     */
    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    /**
     * Updated at date of user.
     */
    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}
