import { PrimaryGeneratedColumn, Column, Entity, OneToOne, JoinColumn, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm'
import { User } from './User';
import { Post } from './Post';
import { Exclude } from 'class-transformer';

@Entity('comment')
export class Comment {
    /**
     * ID of post.
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * Content of post.
     */
    @Column({ type: 'varchar', length: 2048 })
    content: string;

    /**
     * Number of upvotes on post.
     */
    // @Column({ type: 'integer' })
    // upvotes: number;

    /**
     * Number of downvotes on post.
     */
    // @Column({ type: 'integer' })
    // downvotes: number;

    /**
     * Post relation
     */
    @ManyToOne(type => Post, post => post.comments)
    post: Post;

    @Column()
    @Exclude()
    postId: number;

    @ManyToOne((type) => User)
    @JoinColumn()
    user?: User;

    @Column()
    @Exclude()
    userId: number;

    /**
     * Created at date of user.
     */
    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    /**
     * Updated at date of user.
     */
    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}
