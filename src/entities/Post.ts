import { PrimaryGeneratedColumn, Column, Entity, OneToOne, JoinColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm'
import { Comment } from './Comment';
import { Category } from './Category';
import { User } from './User';
import { Exclude } from 'class-transformer';

@Entity('post')
export class Post {
    /**
     * ID of post.
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * Name is category.
     */
    @Column({ type: 'varchar', length: 128 })
    title: string;

    /**
     * Content of post.
     */
    @Column({ type: 'varchar', length: 2048 })
    content: string;

    /**
     * Category relation
     */
    @ManyToOne(type => Category, category => category.posts)
    category: Category;

    @Column()
    @Exclude()
    categoryId: number;

    /**
     * Post relation
     */
    @OneToMany((type) => Comment, comment => comment.post)
    comments?: Comment[];

    @ManyToOne((type) => User)
    @JoinColumn()
    user?: User;

    @Column()
    @Exclude()
    userId: number;

    /**
     * Created at date of user.
     */
    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    /**
     * Updated at date of user.
     */
    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}
