import { PrimaryGeneratedColumn, Column, Entity, CreateDateColumn, UpdateDateColumn } from 'typeorm'

@Entity('report')
export class Report {
    /**
     * ID of report.
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * Content of report.
     */
    @Column({ type: 'varchar', length: 2048 })
    content: string;

    /**
     * User id if the report is for a user.
     */
    @Column({ type: 'integer', nullable: true })
    userId?: number;

    /**
     * Category id if the report is for a category.
     */
    @Column({ type: 'integer', nullable: true })
    categoryId?: number;

    /**
     * Post id if the report is for a post.
     */
    @Column({ type: 'integer', nullable: true })
    postId?: number;

    /**
     * Comment id if the report is for a comment.
     */
    @Column({ type: 'integer', nullable: true })
    commentId?: number;

    /**
     * Created at date of user.
     */
    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    /**
     * Updated at date of user.
     */
    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}
