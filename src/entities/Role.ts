import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm'

@Entity('role')
export class Role {
    /**
     * ID of role.
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * Name of role.
     */
    @Column({ type: 'varchar' })
    name: string;
}
