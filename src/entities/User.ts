import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn } from 'typeorm'
import { Exclude, Expose } from 'class-transformer'
import { Role } from './Role'

@Entity('user')
export class User {
    /**
     * Identifier for user.
     */
    @PrimaryGeneratedColumn()
    id?: number;

    @OneToOne((type) => Role)
    @JoinColumn()
    role?: Role;

    /**
     * Username for user.
     */
    @Column('varchar', { length: 16, unique: true })
    username: string;

    /**
     * First name for user.
     */
    @Column('varchar', { length: 64 })
    firstName: string;

    /**
     * Last name for user.
     */
    @Column('varchar', { length: 64 })
    lastName: string;

    /**
     * Email for user.
     */
    @Column('varchar', { unique: true })
    email: string;

    /**
     * Password for user.
     * Encrypted by bcrypt.
     */
    @Exclude()
    @Column('varchar', { length: 72 })
    password: string;

    /**
     * Created at date of user.
     */
    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    /**
     * Updated at date of user.
     */
    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}
