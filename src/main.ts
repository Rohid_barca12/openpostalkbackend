import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from '@nestjs/common'
import * as rateLimit from 'express-rate-limit'
import * as helmet from 'helmet'

import passport = require('passport');

async function bootstrap () {
  const app = await NestFactory.create(AppModule)
  app.enableCors()
  app.use(passport.initialize())
  app.use(helmet())
  app.use(
    rateLimit({
      windowMs: 5 * 60 * 1000,
      max: 100 // 100 requests every 5 minutes.
    })
  )
  app.useGlobalPipes(new ValidationPipe())
  await app.listen(3000)
}

bootstrap()
