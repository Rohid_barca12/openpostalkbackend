import { Injectable, PipeTransform, Optional, HttpStatus, ArgumentMetadata } from '@nestjs/common'

export interface ParseIntPipeOptions {
  exceptionFactory?: (error: string) => any;
}

/**
 * Defines the built-in ParseInt Pipe
 *
 * @see [Built-in Pipes](https://docs.nestjs.com/pipes#built-in-pipes)
 *
 * @publicApi
 */
@Injectable()
export class OptionalParseIntPipe implements PipeTransform<string> {
  protected exceptionFactory: (error: string) => any;

  constructor (@Optional() options?: ParseIntPipeOptions) {
    options = options || {}
    const {
      exceptionFactory
    } = options

    this.exceptionFactory = exceptionFactory
  }

  /**
   * Method that accesses and performs optional transformation on argument for
   * in-flight requests.
   *
   * @param value currently processed route argument
   * @param metadata contains metadata about the currently processed route argument
   */
  async transform (value: string, metadata: ArgumentMetadata): Promise<number> {
    const isNumeric =
      ['string', 'number'].includes(typeof value) &&
      !isNaN(parseFloat(value)) &&
      isFinite(value as any)
    return isNumeric ? parseInt(value, 10) : undefined
  }
}
