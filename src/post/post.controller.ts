import { Controller, Query, Get, Param, Req, Post, Body, UseGuards } from '@nestjs/common';
import { PostService } from './post.service';
import { OptionalParseIntPipe } from 'src/pipes/optional-parse-int.pipe';
import { CreatePostDto } from 'src/dto/create.post.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('post')
export class PostController {
    constructor (private readonly postService: PostService) {}

    /**
     * Gets all posts.
     *
     * Optional entities paramater can be added
     * to also include all relations into the output body.
     *
     * @param id Identifier for the category this post belongs to.
     * @param entities QueryParamater Number (Optional)
     */
    @Get()
    getAll (@Query('entities', new OptionalParseIntPipe()) entities: number) {     
      return this.postService.getAll(entities)
    }

    /**
     * Gets all posts.
     *
     * Optional entities paramater can be added
     * to also include all relations into the output body.
     *
     * @param id Identifier for the category this post belongs to.
     * @param entities QueryParamater Number (Optional)
     */
    @Get('all/:id')
    getAllWithCategory (@Param('id') id: number, @Query('entities', new OptionalParseIntPipe()) entities: number) {     
      return this.postService.getAllWithCategory(id, entities)
    }

    /**
      * Gets a specific post with the specified id from
      * the route.
      *
      * Optional entities paramater can be added
      * to also include all relations into the output body.
      *
      * @param id Paramter Number
      * @param entities QueryParamater Number (Optional)
      */
    @Get(':id')
    getPost (@Param('id') id, @Query('entities', new OptionalParseIntPipe()) entities: number) {
      return this.postService.getById(id, entities)
    }

    /**
     * Creates a new post and save to the database.
     * Uses CreatePostDto.
     *
     * @param body CreatePostDto.
     */
    @Post()
    @UseGuards(AuthGuard('jwt'))
    create (@Req() req, @Body() body: CreatePostDto) {
      return this.postService.create(req.user.id, body)
    }
    
}
