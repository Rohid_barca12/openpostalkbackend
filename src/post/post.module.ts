import { Module } from '@nestjs/common';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from '../entities/Post';
import { CategoryModule } from 'src/category/category.module';

@Module({
  imports: [
    CategoryModule, 
    TypeOrmModule.forFeature([Post])
  ],
  controllers: [PostController],
  providers: [PostService],
  exports: [PostService]
})
export class PostModule {}
