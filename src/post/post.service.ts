import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post } from '../entities/Post';
import { CreatePostDto } from 'src/dto/create.post.dto';
import { Category } from 'src/entities/Category';
import { CategoryService } from 'src/category/category.service';

@Injectable()
export class PostService {
    constructor (
        @InjectRepository(Post)
        private readonly postRepository: Repository<Post>,
        private readonly categoryService: CategoryService
    ) {}

    /**
     * Gets all posts from the database.
     */
    getAll (entities: number): Promise<Post[]> {
      return entities === 1 ? this.postRepository.find({ relations: ['comments'] }) : this.postRepository.find()
    }
  
    /**
       * Gets all posts with category from the database.
       */
    getAllWithCategory (id: number, entities: number): Promise<Post[]> {
      return this.postRepository.find({
        where: {
          categoryId: id
        },
        relations: entities === 1 ? ['comments'] : undefined
      })
    }

    async create (userId: number, dto: CreatePostDto) {
      const category: Category = await this.categoryService.getById(dto.categoryId, 0);

      // Check if somebody is creating a post for a existing category.
      if (!category) {
        throw new BadRequestException('Category does not exist!')
      }

      const post: Post = await this.getByTitle(dto.title)
  
      // Check if somebody has already created this post with this title.
      if (post) {
        throw new BadRequestException('Title is already in use by another post.')
      }
  
      return this.postRepository.save({
          title: dto.title,
          content: dto.content,
          userId: userId,
          categoryId: dto.categoryId
      })
    }

  /**
     * Gets specified post by id.
     */
  getById (id: number, entities: number): Promise<Post> {
    return entities === 1 ? this.postRepository.findOne({ where: { id: id }, relations: ['comments', 'user', 'category'] }) : this.postRepository.findOne({ id: id })
  }

    /**
     * Gets specified post by title.
     */
  getByTitle (title: string): Promise<Post> {
    return this.postRepository.findOne({ title: title })
  }
}
