import { Controller, UseGuards, Post, Req, Param, Body, Delete, Get, Query } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { CreateReportDto } from 'src/dto/create.report.dto'
import { ReportService } from './report.service'

@Controller('report')
@UseGuards(AuthGuard('jwt'))
export class ReportController {
  constructor(private readonly reportService: ReportService) { }

  /**
   * Gets all reports from the database.
   */
  @Get()
  getAll() {
    return this.reportService.getAll()
  }

  /**
   * Creates a new report and saves it to the database.
   * Uses CreateReportDto.
   *
   * @param reportTarget Parameter String.
   * @param body CreateReportDto.
   */
  @Post()
  create(@Req() req, @Query('reportTarget') reportTarget: string, @Body() body: CreateReportDto) {
    return this.reportService.create(req, reportTarget, body)
  }
}
