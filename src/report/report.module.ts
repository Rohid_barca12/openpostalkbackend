import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UserModule } from 'src/user/user.module'
import { ReportController } from './report.controller'
import { ReportService } from './report.service'
import { Report } from '../entities/Report'

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([Report])
  ],
  controllers: [ReportController],
  providers: [ReportService]
})
export class ReportModule { }
