import { Injectable, BadRequestException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Report } from 'src/entities/Report'
import { CreateReportDto } from 'src/dto/create.report.dto'
import { User } from 'src/entities/User'

@Injectable()
export class ReportService {
  constructor(
    @InjectRepository(Report)
    private readonly reportRepository: Repository<Report>
    // private readonly userService: UserService,
    // private readonly categoryService: CategoryService,
    // private readonly postService: PostService,
    // private readonly commentService: CommentService,
  ) { }

  /**
     * Gets all reports from the database.
     */
  getAll(): Promise<Report[]> {
    return this.reportRepository.find()
  }

  /**
     * Gets a report by id.
     */
  getById(id: number): Promise<Report> {
    return this.reportRepository.findOne({ id: id })
  }

  /**
   * Creates a new report.
   * 
   * @param user User
   * @param reportTarget Report Target
   * @param body CreateReportDto
   */
  create(user: User, reportTarget: string, body: CreateReportDto) {
    if (user === null) {
      throw new BadRequestException("You are not a valid user!")
    }

    switch (reportTarget) {
      case "user":
        return this.createUserReport(body);
      case "category":
        return this.createCategoryReport(body);
      case "post":
        return this.createPostReport(body);
      case "comment":
        return this.createCommentReport(body);
      default:
        throw new BadRequestException("No target was chosen to report!")
    }
  }

  /**
   * Creates a user report.
   * 
   * @param dto CreateReportDto
   */
  private async createUserReport(dto: CreateReportDto) {
    return this.reportRepository.save({
      content: dto.content,
      userId: dto.userId,
    })
  }

  /**
   * Creates a category report.
   * 
   * @param dto CreateReportDto
   */
  private async createCategoryReport(dto: CreateReportDto) {
    return this.reportRepository.save({
      content: dto.content,
      categoryId: dto.categoryId,
    })
  }

  /**
   * Creates a post report.
   * 
   * @param dto CreateReportDto
   */
  private async createPostReport(dto: CreateReportDto) {
    return this.reportRepository.save({
      content: dto.content,
      postId: dto.postId,
    })
  }

  /**
   * Creates a comment report.
   * 
   * @param dto CreateReportDto
   */
  private async createCommentReport(dto: CreateReportDto) {
    return this.reportRepository.save({
      content: dto.content,
      commentId: dto.commentId
    })
  }
}