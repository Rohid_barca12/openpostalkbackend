import { Controller, UseGuards, Post, Req, Body, Delete, Get } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { RoleService } from './role.service'
import { CreateRoleDto } from 'src/dto/create.role.dto'
import { DeleteRoleDto } from 'src/dto/delete.role.dto'

@Controller('role')
@UseGuards(AuthGuard('jwt'))
export class RoleController {
  constructor (private readonly roleService: RoleService) {}

    /**
     * Gets all roles from the database.
     */
    @Get()
  getAll () {
    return this.roleService.getAll()
  }

    /**
     * Creates a new role and saves to the database.
     * Uses CreateRoleDto.
     *
     * @param body CreateRoleDto.
     */
    @Post()
    create (@Req() req, @Body() body: CreateRoleDto) {
      return this.roleService.create(req.user.id, body)
    }

    /**
     * Deletes a exisiting role and removes it from the database.
     * Uses DeleteRoleDto.
     *
     * @param body
     */
    @Delete()
    delete (@Req() req, @Body() body: DeleteRoleDto) {
      return this.roleService.delete(req.user.id, body)
    }
}
