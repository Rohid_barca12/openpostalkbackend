import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Role } from 'src/entities/Role'
import { RoleController } from './role.controller'
import { RoleService } from './role.service'
import { UserModule } from 'src/user/user.module'

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([Role])
  ],
  controllers: [RoleController],
  providers: [RoleService]
})
export class RoleModule { }
