import { Injectable, BadRequestException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Role } from 'src/entities/Role'
import { CreateRoleDto } from 'src/dto/create.role.dto'
import { UserService } from 'src/user/user.service'
import { User } from 'src/entities/User'
import { DeleteRoleDto } from 'src/dto/delete.role.dto'

@Injectable()
export class RoleService {
  constructor (
      @InjectRepository(Role)
      private readonly roleRepository: Repository<Role>,
      private readonly userService: UserService
  ) {}

  /**
     * Gets all roles from the database.
     */
  getAll (): Promise<Role[]> {
    return this.roleRepository.find()
  }

  /**
     * Gets a role by id.
     */
  getById (id: number): Promise<Role> {
    return this.roleRepository.findOne({ id: id })
  }

  /**
     * Gets a role by name.
     */
  getByName (name: string): Promise<Role> {
    return this.roleRepository.findOne({ name: name })
  }

  /**
     * Create new role.
     *
     * TODO: Replace the admin role check with a guard in controller.
     */
  async create (userId: number, dto: CreateRoleDto) {
    const user: User = await this.userService.getById(userId, 1)

    /**
        * Check if user has role priority.
        */
    if (!user.role || user.role.name !== 'Admin') {
      throw new BadRequestException('You do not have permission to create a role.')
    }

    const role: Role = await this.getByName(dto.name)

    // Check if this is a role.
    if (role) {
      throw new BadRequestException('This role does already exists.')
    }

    this.roleRepository.save({
      name: dto.name
    })
  }

  /**
     * Delete existing role.
     *
     * TODO: Replace the admin role check with a guard in controller.
     */
  async delete (userId: number, dto: DeleteRoleDto) {
    const user: User = await this.userService.getById(userId, 1)

    if (!user.role || user.role.name !== 'Admin') {
      throw new BadRequestException('You do not have permission to delete a role.')
    }

    const role: Role = await this.getById(dto.id)

    // Check if this is a category.
    if (!role) {
      throw new BadRequestException('This role does not exist.')
    }

    this.roleRepository.delete(dto.id)
  }
}
