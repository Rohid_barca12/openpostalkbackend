import { Controller, UseGuards, Get, Req, Param, Patch, Body, BadRequestException, UnauthorizedException, Query, Delete } from '@nestjs/common'
import { UserService } from './user.service'
import { User } from 'src/entities/User'
import { UpdatePasswordDto } from 'src/dto/update.password.dto'
import { OptionalParseIntPipe } from 'src/pipes/optional-parse-int.pipe'
import { AuthGuard } from '@nestjs/passport'
import * as bcrypt from 'bcrypt'

@Controller('user')
@UseGuards(AuthGuard('jwt'))
export class UserController {
  constructor (private readonly userService: UserService) {}

  /**
   * Gets all users.
   *
   * Optional entities paramater can be added
   * to also include all relations into the output body.
   *
   * @param entities QueryParamater Number (Optional)
   */
  @Get()
  getAll (@Query('entities', new OptionalParseIntPipe()) entities: number) {
    return this.userService.getAll(entities)
  }

  /**
   * Gets current user that is logged in from
   * the user service with the id.
   * Requires bearer authorization with JWT.
   *
   * Optional entities paramater can be added
   * to also include all relations into the output body.
   *
   * @param req Request
   * @param entities QueryParamater Number (Optional)
   */
  @Get('current')
  getCurrentUser (@Req() req, @Query('entities', new OptionalParseIntPipe()) entities: number) {
    return this.userService.getById(req.user.id, entities)
  }

  /**
   * Gets a specific user with the specified id from
   * the route.
   *
   * Optional entities paramater can be added
   * to also include all relations into the output body.
   *
   * @param id Paramter Number
   * @param entities QueryParamater Number (Optional)
   */
  @Get(':id')
  getUser (@Param('id') id, @Query('entities', new OptionalParseIntPipe()) entities: number) {
    return this.userService.getById(id, entities)
  }

  /**
   * Gets current user and updates settings with
   * the update settings dto.
   * Requires bearer authorization with JWT.
   *
   * @param req Request
   * @param body Body
   */
  @Patch('current/updateSettings')
  updateSettings (@Req() req, @Body() body) {

  }

  /**
   * Gets current user and updates password with
   * the update password dto.
   * Requires bearer authorization with JWT.
   *
   * TODO: Update to service
   *
   * @param req Request
   * @param password UpdatePasswordDto
   */
  @Patch('current/updatePassword')
  async updatePassword (@Req() req, @Body() password: UpdatePasswordDto) {
    const user: User = await this.userService.getById(req.id, 0)

    if (user == null) {
      throw new BadRequestException('User does not exist!')
    }

    if (!bcrypt.compareSync(password.oldPassword, user.password)) {
      throw new BadRequestException('Wrong password!')
    }

    if (password.oldPassword === password.newPassword || password.oldPassword === password.confirmNewPassword) {
      throw new BadRequestException('Same password!')
    }

    if (password.newPassword !== password.confirmNewPassword) {
      throw new BadRequestException('Passwords do not match!')
    }

    user.password = await bcrypt.hash(password.newPassword, 12)
    return this.userService.save(user)
  }

  /**
   * Gets the specified user and deletes it from the database.
   * Requires bearer token and admin role.
   *
   * @param id UserID
   */
  @Delete(':id')
  async delete (@Req() req, @Param('id') id) {
    this.userService.delete(req.user.id, id)
  }
}
