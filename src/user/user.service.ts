import { Injectable, BadRequestException } from '@nestjs/common'
import { User } from 'src/entities/User'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

@Injectable()
export class UserService {
  constructor (
      @InjectRepository(User)
      private readonly userRepository: Repository<User>
  ) {}

  /**
     * Gets all users from the database.
     */
  getAll (entities: number): Promise<User[]> {
    return entities === 1 ? this.userRepository.find({ relations: ['role'] }) : this.userRepository.find()
  }

  /**
     * Gets specified user with id.
     */
  getById (id: number, entities: number): Promise<User> {
    return entities === 1 ? this.userRepository.findOne(id, { relations: ['role'] }) : this.userRepository.findOne(id)
  }

  /**
   * Gets specified user with id.
   */
  getByIdSecure (id: number, entities: number): Promise<User> {
    return entities === 1 ? this.userRepository.findOne(id, { relations: ['role'] }) : this.userRepository.findOne(id)
  }

  /**
     * Gets specified user with username.
     */
  getByUsername (username: string): Promise<User> {
    return this.userRepository.findOne({ username: username })
  }

  /**
     * Gets specified user with email.
     */
  getByEmail (email: string): Promise<User> {
    return this.userRepository.findOne({ email: email })
  }

  /**
     * Saves user.
     */
  save (user: User): Promise<User> {
    return this.userRepository.save(user)
  }

  /**
   * Delete existing user.
   *
   * TODO: Replace the admin role check with a guard in controller.
   */
  async delete (userId: number, targetId: number) {
    const user: User = await this.getById(userId, 1)

    if (!user.role || user.role.name !== 'Admin') {
      throw new BadRequestException('You do not have permission to delete a user.')
    }

    const targetUser = await this.getById(targetId, 1)

    if (!targetUser) {
      throw new BadRequestException('User does not exist!')
    }

    return this.userRepository.delete(targetId)
  }
}
